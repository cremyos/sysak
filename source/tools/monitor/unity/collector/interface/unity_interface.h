//
// Created by 廖肇燕 on 2023/2/15.
//

#ifndef UNITY_UNITY_INTERFACE_H
#define UNITY_UNITY_INTERFACE_H

void set_unity_proc(const char *path);
void set_unity_sys(const char *path);
char *get_unity_proc(void);
char *get_unity_sys(void);

#endif //UNITY_UNITY_INTERFACE_H
