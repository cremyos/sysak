//
// Created by muya.
//

#ifndef UNITY_NUMAINFO_H
#define UNITY_NUMAINFO_H

#include "../plugin_head.h"

int init(void * arg);
int call(int t, struct unity_lines* lines);
void deinit(void);

#endif //UNITY_NUMAINFO_H
