//
// Created by muya.
//

#ifndef UNITY_GPUINFO_H
#define UNITY_GPUINFO_H

#include "../plugin_head.h"

int init(void * arg);
int call(int t, struct unity_lines* lines);
void deinit(void);

#endif //UNITY_GPUINFO_H
